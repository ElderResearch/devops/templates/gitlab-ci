# GitLab CI

Template files for GitLab CI/DC jobs

<hr/>
<b><big>WARNING:</big></b> This repository is <b>publicly accessible</b> so that the build runners can access the template YML files no sensitive ERI code or information should be stored here
<hr/>


## Using the R package CI/CD templates

The R package CI/CD templates make it mostly automatic to test a
package, deploy it to ERI's Artifactory, and generate a
[pkgdown][] website.

#### Testing

By modifying the `rules`, we can control when a package is tested.

```yaml
include:
  - project: "ElderResearch/devops/templates/gitlab-ci"
    file: 
      - R.test.gitlab-ci.yml
  
test:
  extends: .test
  rules:
    - when: always
```

#### Deploy to Artifactory

In this example we've limited deployment to version-like tags,
e.g., "v0.1.0".

```yaml
include:
  - project: "ElderResearch/devops/templates/gitlab-ci"
    file:
      - R.deploy.gitlab-ci.yml

deploy:
  extends: .deploy
  rules:
    - if: $CI_COMMIT_TAG =~ /^v[0-9.+-]+$/
      when: always
    - when: never
```

#### Publish a pkgdown website

These websites are available via GitLab Pages, found at 
`https://elderresearch.gitlab.io/path/to/your/project/`.

```yaml
include:
  - project: "ElderResearch/devops/templates/gitlab-ci"
    file:
      - R.pkgdown.gitlab-ci.yml

pages:
  extends: .pages
```

#### Full configuration example

In this example, we

- Test on every push;
- Deploy when new versions are tagged; and
- Generate package documentation on `main` or when `/pkgdown` is
  in the commit message.

```yaml
include:
  - project: "ElderResearch/devops/templates/gitlab-ci"
    file:
      - R.test.gitlab-ci.yml
      - R.deploy.gitlab-ci.yml
      - R.pkgdown.gitlab-ci.yml

test: 
  extends: .test

deploy:
  extends: .deploy
  rules:
    - if: $CI_COMMIT_TAG =~ /^v[0-9.+-]+$/
      when: always
    - when: never

pages:
  extends: .pages
  rules:
  - if: $CI_COMMIT_MESSAGE =~ /\/pkgdown/
    when: always
  - if: $CI_COMMIT_BRANCH == "main"
    when: always
  - when: never
```


[pkgdown]: https://pkgdown.r-lib.org
